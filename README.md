# Basejs
A base to node.js projects

## Prerequisites
Make sure you have [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/gettingstarted/) installed.

_In some systems docker will require super user permissions_

## Getting Started

1. fork the BaseJS repository.
2. clone the forked repository.  
3. inside the repository root folder, init submodules with command `git submodule update --init`.
4. make a copy of `.env` with command `cp backend/env.example ./.env`
5. edit `.env` file defining the values of keys _FACEBOOK_APP_SECRET_, _FACEBOOK_APP_ID_ and _APP_KEY_;

## Development environment
The development environment consists of four services: _ingress_, _frontend_, _backend_ and _database_.

- **ingress** is an nginx container with a self-signed certificate that serves the static files created by the _frontend_. When no static file is found the container forwards the request to the backend;
- **frontend** is a Node.js container with _vue-cli_ installed. The entrypoint script runs `npm run dev-watch`, that rebuilds the frontend code when it changes;
- **backend** is a Node.js container with pm2 and AdonisJs installed. The entrypoint script runs the adonis migrations and serves the application with pm2 in dev mode, that reloads the application when it changes;
- **database** is a PostgresSQL container.

### Starting development environment
Before starts the development environment for the firts time, create the ssl keys for the ingress container:
```
$ cd ingress
$ ./generate-dev-keys.sh
$ cd ..
```

Start containers with docker-compose:
```
$ docker-compose -f docker-compose.dev.yml up
```

now you can access **https://localhost/**

## Production environment
The production environment consists of four services: _frontend_, _backend_ and _database_ that initialize with the directive [restart: always](https://docs.docker.com/compose/compose-file/compose-file-v2/#restart)
- **ingress** is an nginx container that serves the static files created by the _frontend_ in port 8888. When no static file is found the container forwards the request to the backend;
- **frontend** is a Node.js container with vue-cli installed. entrypoint runs `npm run prod` that builds the frontend code;
- **backend** is a Node.js container with pm2 and adonisjs installed. entrypoint runs the adonis migrations and serves application with pm2;
- **database** is a PostgresSQL container.


### Starting production environment
```
docker-compose -f docker-compose.prod.yml up
```

**Note**: You need to configure a reverse proxy with a valid certificate and forward the requests to the ingress container in the port 8888.

## Bash commands
In unix systems, you can activate some useful bash alias running one of the follow command in the root folder of your project:
```
# for development
$ . ./activate

# for production
$ . ./activate prod
```

The above command will create the follow alias:
```
# base bjs alias - $DOCKERFILE is 'prod' or 'dev'
alias bjs-dc="docker-compose -f $DOCKERFILE"

#commands that uses bjs-dc
alias bjs-up="bjs-dc up"
alias bjs-down="bjs-dc down"
alias bjs-build="bjs-dc build"
alias bjs-exec="bjs-dc exec"

#commands that uses bjs-exec
alias bjs-front="bjs-exec frontend"
alias bjs-back="bjs-exec backend"
alias bjs-db="bjs-exec database"

#commands that runs inside frontend container
alias bjs-vue="bjs-front vue"
alias bjs-front-shell="bjs-front bash"
alias bjs-front-npm="bjs-front npm"

#commands that runs inside backend container
alias bjs-adonis="bjs-back adonis"
alias bjs-back-shell="bjs-back bash"
alias bjs-back-npm="bjs-back npm"

#commands that runs inside database container
alias bjs-psql="bjs-exec database psql -U db_user -d db_name"
alias bjs-pg_dump="bjs-exec database pg_dump -U db_user -d db_name"
```

### Usage examples

Accessing the PostgreSQL console
```
$ bjs-psql
```

Installing fb package in backend container
```
bjs-back-npm install fb --save
#or
bjs-back npm install fb --save
```

Running Adonis help
```
bjs-adonis --help
```

Installing Adonis Auth package
```
bjs-adonis install @adonisjs/auth
```

Creating a new model with adonis
```
bjs-adonis create:model ModelName
```

Running Adonis migrations
```
bjs-adonis migration:run
```
